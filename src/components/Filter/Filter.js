import 'react-dates/initialize';
import React, { Component } from 'react';
import './Filter.scss'
import PropTypes from 'prop-types'
import ReactAutocomplete from 'react-autocomplete'

import { DateRangePicker, SingleDatePicker } from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import moment from 'moment'

class Tab_menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          selected: false,
        };

        // This binding is necessary to make `this` work in the callback
        this.handleClick = this.handleClick.bind(this);
    }
    static propTypes = {
        text: PropTypes.string,
    }
    handleClick() {
        console.log("tabmenu");
        this.props.changeRoute();
        var tab_state = document.getElementsByClassName('flight__filter-tab-menu');
        for (var i = tab_state.length - 1; i >= 0; i--) {
          tab_state[i].classList.remove('active');
        }
        this.setState({
            selected:false
        });

        setTimeout(function(){
            this.setState({
                selected:true
            });
        }.bind(this),0.1);
      }
    render() {
        return (
            <li className={"flight__filter-tab-menu "+ (this.props.firststate) +" "+ (this.state.selected ? 'active' : '')} id="once" onClick={this.handleClick}>{this.props.text}</li>
        );
    }
}

const today = moment(new Date());

class Place extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
          defaultValue:  this.props.default_text,
        }
    }
    static propTypes = {
        default_text: PropTypes.string,
    }

    render() {
    return (
      <ReactAutocomplete
        value={this.props.default_text}
        wrapperStyle={{ 'display': 'block' }}
        menuStyle={{ 'position': 'absolute',
                    'width': '380px',
                        'fontSize': '14px',
                        'backgroundColor': '#ffffff',
                        'borderTop': 'solid 1px rgba(0, 0, 0, 0.12)',
                        'borderBottom': 'solid 1px rgba(0, 0, 0, 0.12)',
                        'padding': '0',
                        'color': 'rgba(0, 0, 0, 0.54)',
                        'fontWeight': '600',
                        'marginBottom': '15px',
                        'zIndex':'2',
                        'left':'0',
                        'top':'120%',
                        'overflow':'auto',
                        'maxHeight': '300px',
                        'boxShadow': '0 8px 10px 0 rgba(0, 0, 0, 0.15), 0 -3px 6px 0 rgba(0, 0, 0, 0.15)',
                        'borderRadius': '6px',
        }}
        defaultValue={'asd'}
        items={[
          { label: "Denpasar", country: "Indonesia" },
          { label: "Jakarta", country: "Indonesia" },
          { label: "Makasar", country: "Indonesia"},
          { label: "Medan", country: "Indonesia" },
          { label: "Surabaya", country: "Indonesia" },
          { label: "Yogyakarta", country: "Indonesia" },
          { label: "Singapore", country: "Singapura" },
          { label: "Hanoi", country: "Vietnam" },
          { label: "Ho Chi Minh", country: "Vietnam" },
          { label: "DJohor Bahru", country: "Malaysia" },
          { label: "Kota Kinabalu", country: "Malaysia" },
          { label: "Kuala Lumpur", country: "Malaysia"},
          { label: "Kuching", country: "Malaysia" },
          { label: "Penang", country: "Malaysia" },
          { label: "Bangkok", country: "Thailand" },
          { label: "Krabi", country: "Thailand" },
          { label: "Phuket", country: "Thailand" },
          { label: "Guangzhou", country: "Hong Kong, Macau & China" },
          { label: "Hong Kong", country: "Hong Kong, Macau & China" },
          { label: "Macau", country: "Hong Kong, Macau & China" },
          { label: "Jeju", country: "Jepang & Korea Selatan" },
          { label: "Seoul", country: "Jepang & Korea Selatan" },
          { label: "Tokyo", country: "Jepang & Korea Selatan" },
          { label: "Adelaide", country: "Australia" },
          { label: "Albury", country: "Australia" },
          { label: "Brisbane", country: "Australia" },
          { label: "Canberra", country: "Australia" },
          { label: "Perth", country: "Australia" },
          { label: "Sydney", country: "Australia" },
          { label: "London", country: "United Kingdom" },
          { label: "Aberdeen", country: "United Kingdom" },
          { label: "Belfast", country: "United Kingdom" },
          { label: "Birmingham", country: "United Kingdom" },
          { label: "Bristol", country: "United Kingdom" },
          { label: "Cardiff", country: "United Kingdom" },
        ]}
        shouldItemRender={(item, defaultValue) => item.label.toLowerCase().indexOf(defaultValue.toLowerCase()) > -1}
        getItemValue={item => item.label}
        renderItem={(item, highlighted) =>
          <div
            key={item.label}
            style={{ backgroundColor: highlighted ? '#eee' : 'transparent',
                    'padding': '15px 20px',
                    'cursor': 'pointer',
                     }}
          >
            {item.label}
          </div>
        }
        defaultValue={this.state.defaultValue}
        onChange={e => this.setState({ defaultValue: e.target.defaultValue })}
        onSelect={defaultValue => this.setState({ defaultValue })}
      />
    )
    }

}

const focusInCurrentTarget = ({ relatedTarget, currentTarget }) => {
  if (relatedTarget === null) return false;

  var node = relatedTarget.parentNode;

  while (node !== null) {
    if (node === currentTarget) return true;
    node = node.parentNode;
  }

  return false;
}

export default class Filter extends Component {
    constructor(props) {
        super(props);
        this.state = {
          roundtrip: false,
          date: today,
          startDate: today,
          endDate: today,
          roundtrip: false,
          passenger:false,
          flight_class:false,
          passenger_amount: {
            adult: 1,
            child: 0,
            baby: 0,
          },
          passenger_input: '1 Dewasa',
          baby_limit: true,
          adult_limit: true,
          child_limit: true,
          passenger_class : 'Economy',
          class_economy : 'active',
          class_business : '',
          class_premium : '',
        };
    }
    Depature() {
      this.setState({
          roundtrip:false
      });
    }
    Return() {
      this.setState({
          roundtrip:true
      });
    }
    Passenger_show() {
      this.setState({
          passenger:true
      });
    }
    Passenger_hide(e) {
      if (!focusInCurrentTarget(e)) {
        this.setState({
          passenger:false
        });
      }
    }
    Class_show() {
      this.setState({
          flight_class:true
      });
    }
    passengerValidation(adult,child,baby){
      if(this.state.passenger_amount.baby + baby > this.state.passenger_amount.adult){
        this.setState({
          baby_limit: false,
        });
        setTimeout(() => {
          this.setState({
            baby_limit: true,
          });
        },5000);
        return true;
      }else if(this.state.passenger_amount.child + child > 8 ){
        this.setState({
          child_limit: false,
        });
        setTimeout(() => {
          this.setState({
            child_limit: true,
          });
        },5000);
        return true;
      }else if(this.state.passenger_amount.adult + adult > 8 ){
        this.setState({
          adult_limit: false,
        });
        setTimeout(() => {
          this.setState({
            adult_limit: true,
          });
        },5000);
        return true;
      }
      return false;
    }
    addPassenger(adult,child,baby){
      if(this.passengerValidation(adult,child,baby)){
        return;
      }

      this.setState({
        passenger_amount: {
          adult: this.state.passenger_amount.adult + adult,
          child: this.state.passenger_amount.child + child,
          baby: this.state.passenger_amount.baby + baby,
        },
      }, () => {
        this.changePassengerText();
      });
    }
    changePassengerText(){
      var text="";
      if(this.state.passenger_amount.adult > 0){
        text = text + this.state.passenger_amount.adult + " Dewasa";
      }
      if(this.state.passenger_amount.child > 0){
        text = text + ", " + this.state.passenger_amount.child + " Anak";
      }
      if(this.state.passenger_amount.baby > 0){
        text = text + ", " + this.state.passenger_amount.baby + " Bayi";
      }
      this.setState({
        passenger_input: text,
      });
    }
    changeClass(value){
      if(value == "Economy"){
        this.setState({
          passenger_class: value,
          class_premium: '',
          class_business: '',
          class_economy: 'active',
          flight_class: false,
        });
      }else if(value == "Business"){
        this.setState({
          passenger_class: value,
          class_premium: '',
          class_business: 'active',
          class_economy: '',
          flight_class: false,
        });
      }else if(value == "Premium Economy"){
        this.setState({
          passenger_class: value,
          class_premium: 'active',
          class_business: '',
          class_economy: '',
          flight_class: false,
        });
      }
    }
    render() {
        return (
          <div className="flight__filter-card">
            <ul className="flight__filter-tab fs-0">
                <Tab_menu text="Sekali Jalan" firststate="active" changeRoute={this.Depature.bind(this)}/>
                <Tab_menu text="Pulang Pergi" changeRoute={this.Return.bind(this)}/>
            </ul>
            <hr className="u-mt-1 u-mb32" />
            <div className="flight__filter-tab-content fs-0">
                <div className="flight__filter-input line-1">
                    <div className="fs-12 u-left-align" >
                        Dari
                    </div>
                    <Place default_text={'Jakarta'}/>
                    <div className="flight__filter-origin fs-0" hidden="true" id="origin-content">
                        <div className="flight__filter-origin__title">
                            <b>Pilih Asal Penerbangan Anda</b>
                        </div>
                        <div className="flight__filter-origin__left-section">
                            <ul>
                                <li className="flight__list-tab active" id="flight__list-tab1">Populer</li>
                                <li className="flight__list-tab" id="flight__list-tab2">Indonesia</li>
                                <li className="flight__list-tab" id="flight__list-tab3">ASEAN</li>
                                <li className="flight__list-tab" id="flight__list-tab4">China, Jepang, Hongkong, Korea Selatan</li>
                                <li className="flight__list-tab" id="flight__list-tab5">Australia dan Selandia Baru</li>
                            </ul>
                        </div>
                        <div className="flight__filter-origin__right-section">
                            <ul>

                            </ul>
                        </div>
                    </div>
                    <div className="error-message" hidden="true">
                            Departure and destination city must differ
                    </div>
                </div>
                <div className="flight__filter-input line-2">
                    <div className="flight__icon-roundtrip" id="switch"></div>
                </div>
                <div className="flight__filter-input line-3">
                    <div className="fs-12 u-left-align">
                        Tujuan
                    </div>
                    <Place default_text={'London'}/>
                    <div className="flight__filter-origin fs-0" hidden="true" id="destination-content">
                        <div className="flight__filter-origin__title">
                            <b>Pilih Asal Penerbangan Anda</b>
                        </div>
                        <div className="flight__filter-origin__left-section">
                            <ul>
                                <li className="flight__list-tab active" id="flight__list-tab1">Populer</li>
                                <li className="flight__list-tab" id="flight__list-tab2">Indonesia</li>
                                <li className="flight__list-tab" id="flight__list-tab3">ASEAN</li>
                                <li className="flight__list-tab" id="flight__list-tab4">China, Jepang, Hongkong, Korea Selatan</li>
                                <li className="flight__list-tab" id="flight__list-tab5">Australia dan Selandia Baru</li>
                            </ul>
                        </div>
                        <div className="flight__filter-origin__right-section">
                            <ul>

                            </ul>
                        </div>
                    </div>
                </div>
                {this.state.roundtrip==false &&
                    <div className="flight__filter-input line-4 custom__datepicker">
                        <div className="fs-12 u-left-align">
                            Berangkat
                        </div>
                        <SingleDatePicker
                          hideKeyboardShortcutsPanel= {true}
                          displayFormat="DD-MM-YYYY"
                          date={this.state.date} // momentPropTypes.momentObj or null
                          onDateChange={date => this.setState({ date })} // PropTypes.func.isRequired
                          focused={this.state.focused} // PropTypes.bool
                          onFocusChange={({ focused }) => this.setState({ focused })} // PropTypes.func.isRequired
                        />
                    </div>
                }
                {this.state.roundtrip==true &&
                    <div className="flight__filter-input line-4 custom__daterangepicker">
                        <DateRangePicker
                          hideKeyboardShortcutsPanel= {true}
                          endDatePlaceholderText= ''
                          displayFormat="DD-MM-YYYY"
                          startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                          endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                          onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate })} // PropTypes.func.isRequired,
                          focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                          onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
                        />
                    </div>
                }
                <div className="flight__filter-input line-5" onBlur={this.Passenger_hide.bind(this)}>
                    <div className="fs-12 u-left-align">
                        Penumpang
                    </div>
                    <input type="text" className="text-input u-mt1" value={this.state.passenger_input} id="passenger" onClick={this.Passenger_show.bind(this)}  readOnly/>
                    {this.state.passenger==true &&
                        <div className="flight__filter-passenger">
                            <div className="row fs-0">
                                <div className="u-col-7 u-inline-block">
                                    <span className="fs-14"><b>Dewasa</b></span><br />
                                    <span className="fs-12">(Lebih dari 12 tahun)</span>
                                </div>
                                <div className="u-col-5 u-inline-block u-right-align">
                                    <button onClick={() => this.addPassenger(-1,0,0)}>
                                      <span className="minus">-</span>
                                    </button>
                                    <input type="text" readOnly value={this.state.passenger_amount.adult} id="adult" />
                                    <button onClick={() => this.addPassenger(1,0,0)}>
                                      <span className="plus">+</span>
                                    </button>
                                </div>
                            </div>

                            <span className="color--error fs-10 error-passenger" id="error-adult" hidden={this.state.adult_limit}>Jumlah maksimum penumpang 8 orang</span>

                            <div className="row fs-0 u-mt2">
                                <div className="u-col-7 u-inline-block">
                                    <span className="fs-14"><b>Anak</b></span><br/>
                                    <span className="fs-12">(2 - 12 tahun)</span>
                                </div>
                                <div className="u-col-5 u-inline-block u-right-align">
                                    <button onClick={() => this.addPassenger(0,-1,0)}>
                                      <span className="minus">-</span>
                                    </button>
                                    <input type="text" readOnly value={this.state.passenger_amount.child} id="child" />
                                    <button onClick={() => this.addPassenger(0,1,0)}>
                                      <span className="plus">+</span>
                                    </button>
                                </div>
                            </div><span className="color--error fs-10 error-passenger" id="error-child" hidden={this.state.child_limit}>Jumlah maksimum penumpang 8 orang</span>

                            <div className="row fs-0 u-mt2">
                                <div className="u-col-7 u-inline-block">
                                    <span className="fs-14"><b>Bayi</b></span><br/>
                                    <span className="fs-12">(Kurang dari 2 Tahun)</span>
                                </div>
                                <div className="u-col-5 u-inline-block u-right-align">
                                    <button onClick={() => this.addPassenger(0,0,-1)}>
                                      <span className="minus">-</span>
                                    </button>
                                    <input type="text" readOnly value={this.state.passenger_amount.baby} id="baby" />
                                    <button onClick={() => this.addPassenger(0,0,1)}>
                                      <span className="plus">+</span>
                                    </button>
                                </div>
                            </div><span className="color--error fs-10 error-passenger" id="error-baby" hidden={this.state.baby_limit}>Jumlah bayi tidak boleh lebih banyak dari orang dewasa</span>
                        </div>
                    }
                </div>
                <div className="flight__filter-input line-6">
                    <div className="fs-12 u-left-align">
                        Kelas
                    </div>
                    <input type="text" className="text-input u-mt1" id="className" value={this.state.passenger_class} onClick={this.Class_show.bind(this)} readOnly/>
                    {this.state.flight_class==true &&
                        <div className="flight__filter-class">
                            <div className="row fs-0" onClick={() => this.changeClass('Economy')}>
                                <div className={"u-inline-block option " + this.state.class_economy}>
                                    <span className="fs-14"><b>Economy</b></span>
                                </div>
                            </div>
                            <hr />
                            <div className="row fs-0" onClick={() => this.changeClass('Business')}>
                                <div className={"u-inline-block option " + this.state.class_business}>
                                    <span className="fs-14"><b>Business</b></span>
                                </div>
                            </div>
                            <hr />
                            <div className="row fs-0" onClick={() => this.changeClass('Premium Economy')}>
                                <div className={"u-inline-block option " + this.state.class_premium}>
                                    <span className="fs-14"><b>Premium Economy</b></span>
                                </div>
                            </div>

                        </div>
                    }
                </div>
            </div>
            <div className="flight__filter-tab-content fs-0">
                <div className="u-col-6 u-inline-block">
                </div>
                <div className="u-col-6 u-inline-block u-right-align">
                    <button className="primary__btn--large backcolor--orange flight__filter-button">Cari Tiket</button>
                </div>
            </div>
          </div>
        );
    }
}

