import React, { Component } from 'react';
import './Footer.scss'

export default class componentName extends Component {
	render() {
		return (
			<div className="footer footer-noflex">
				<div className="footer__lower">
						<div className="container">
								<div className="u-clearfix">
										<div className="footer__cp-container">
												<ul>
													<li className="footer__cp">
														<img src="https://ecs7.tokopedia.net/img/footer/toped.png" width="40" className="mr-5" alt="Tokopedia Logo" />
													</li>
													<li className="footer__cp u-ml1">
														<div>
																&copy; 2009-2017, PT Tokopedia
														</div>
													</li>
												</ul>
										</div>
										<div className="footer__help-container">
											<a className="footer__help" target="_blank" href="https://www.tokopedia.com/contact-us.pl">Bantuan</a>&nbsp;|&nbsp;
											<a className="footer__help" target="_blank" href="https://www.tokopedia.com/bantuan/217160206-kendala-pengisian-pulsa/">FAQ</a>&nbsp;|&nbsp;
											<a className="footer__help" target="_blank" href="https://www.tokopedia.com/bantuan/218066503-syarat-dan-ketentuan-pembelian-pulsa/" title="Syarat &amp; Ketentuan">Syarat &amp; Ketentuan</a>
										</div>
								</div>
						</div>
				</div>
			</div>
		);
	}
}

