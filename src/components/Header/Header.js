import React, { Component } from 'react';
import './Header.scss'

class Header extends Component {

  props: {
    profileVisible: bool,
    bridgeWidgetVisible: bool,
    onHoverUserIcon: func,
    onLeaveUserIcon: func,
    onLeaveBridgeIcon: func,
    onHoverBridgeIcon: func
	}

  render() {
    return (
      <header className='dm-header'>
        <nav className='dm-nav u-clearfix'>
          <a className='dm-nav__logo dm-logo--pulsa'>Tokopedia Pulsa</a>
          <ul className='dm-nav__menu u-clearfix'>
            <li className='dm-nav__item'>
              <a className='dm-nav__link' href='#'>Promo</a>
            </li>
            <li className='dm-nav__item'>
              <a className='dm-nav__link' href='#'>Daftar Produk</a>
            </li>
            <li className="dm-nav__item loggedin">
                <a className="dm-nav__link" href="https://pulsa-staging.tokopedia.com/order-list">Daftar Transaksi</a>
            </li>
            <li className="dm-nav__item loggedin">
                <a className="dm-nav__link" href="https://pulsa-staging.tokopedia.com/subscribe">Langganan</a>
            </li>
            <li className="dm-nav__item notloggedin" style={{"display" : "none"}}>
                <a className="dm-nav__link dm-nav__link--outline" href="https://pulsa-staging.tokopedia.com/login?theme=default">Masuk</a>
            </li>
            <li className='dm-nav__item loggedin dropdown-hover m-0' onMouseLeave={this.props.onLeaveUserIcon}>
              <div className='dm-avatar pl-15 pr-15'>
                <div className='dm-avatar__img-container'
                     onMouseOver={this.props.onHoverUserIcon}>
                  <img className='dm-avatar__img userimg' width='27' height='27' src='https://imagerouter-staging.tokopedia.com/image/v1/u/5480069/user_thumbnail/desktop' />
                </div>
                {this.props.profileVisible &&
                  <div className="dm-submenu">
                    <div className="dm-info">
                      <h6 className="dm-info__title">Profile Saya</h6>
                      <a href="https://staging.tokopedia.com/people/5480069" className="fullname-link">
                        <p className="dm-info__name fullname fs-13">Victoria Lius</p>
                      </a>
                      <hr className="dm-submenu__line" />
                      <div className="dm-info__value">
                        <a href="https://www.tokocash.com/topup" className="dm-info__tokocash-link">
                          <h6 className="dm-info__title">TokoCash</h6>
                          <p className="dm-info__amount fs-13 tokocash-balance">Rp 1.628.291</p>
                        </a>
                        <span className="activate-tokocash none"><a href="https://accounts-staging.tokopedia.com/wallet/activation?ld=https%3A%2F%2Fpulsa-staging.tokopedia.com%2F"><button className="btn dm-info__button fs-12 bg-orange text-center white small">Aktivasi</button></a></span>
                        <span className="topup-tokocash"><a href="https://pulsa-staging.tokopedia.com/tokocash"><button className="btn dm-info__button fs-12 bg-orange text-center white small">Topup</button></a></span>
                      </div>
                      <hr className="dm-submenu__line" />
                    </div>
                    <ul>
                      <li className="dm-submenu__item">
                        <a href="https://pulsa-staging.tokopedia.com/logout" className="dm-submenu__link dm-submenu__text dm-submenu__link__logout">
                            Keluar
                        </a>
                      </li>
                    </ul>
                  </div>

                }

              </div>
            </li>
            <li className='dm-nav__item m-0'
                onMouseOver={this.props.onHoverBridgeIcon}
                onMouseLeave={this.props.onLeaveBridgeIcon}>
              <div className='bridge-widget bridge-widget-hover'>
                <button className='bw-button'>Expander</button>
              </div>
              { this.props.bridgeWidgetVisible &&
                <div className="bw-container">
                  <div className="bw-item">
                      <a href="https://staging.tokopedia.com/">
                          <div className="bw-icon__wrapper">
                              <div className="bw-icon bw-icon-toped"></div>
                          </div>
                          <p className="bw-icon__text">Jual Beli Online</p>
                          <span className="clear-b"></span>
                      </a>
                  </div>
                  <div className="bw-item">
                      <a href="https://www.tokopedia.com/official-store/?utm_source=pulsa&amp;utm_medium=icon">
                          <div className="bw-icon__wrapper">
                              <div className="bw-icon bw-icon-official"></div>
                          </div>
                          <p className="bw-icon__text">Official Store</p>
                          <span className="clear-b"></span>
                      </a>
                  </div>
                  <div className="bw-item">
                      <a href="https://staging.tokopedia.com/pulsa/">
                          <div className="bw-icon__wrapper">
                              <div className="bw-icon bw-icon-pulsa"></div>
                          </div>
                          <p className="bw-icon__text">Product Digital</p>
                          <span className="clear-b"></span>
                      </a>
                  </div>
                  <div className="bw-item">
                      <a href="https://tiket.tokopedia.com/?utm_source=pulsa&amp;utm_medium=icon">
                          <div className="bw-icon__wrapper">
                              <div className="bw-icon bw-icon-tiket"></div>
                          </div>
                          <p className="bw-icon__text">Tiket Kereta</p>
                          <span className="clear-b"></span>
                      </a>
                  </div>
                  <div className="bw-item">
                      <a href="https://staging.tokopedia.com/bantuan/">
                          <div className="bw-icon__wrapper">
                              <div className="bw-icon bw-icon-help"></div>
                          </div>
                          <p className="bw-icon__text">Bantuan</p>
                          <span className="clear-b"></span>
                      </a>
                  </div>
                </div>
              }

            </li>
          </ul>
        </nav>
      </header>
    );
  }
}

export default Header
