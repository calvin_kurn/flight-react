import React from 'react'
import './TempView.scss'
import Carousel from 'nuka-carousel'

import img1 from './1.jpg'
import img2 from './2.jpg'
import img3 from './3.jpg'

export const TempView = () => (
  <div className="container_temp">
     <Carousel slideWidth={0.8} cellSpacing={20}>
        <img src={img1} />
        <img src={img2} />
        <img src={img3} />
        <img src={img1} />
        <img src={img2} />
        <img src={img3} />
      </Carousel>
  </div>
)

export default TempView
