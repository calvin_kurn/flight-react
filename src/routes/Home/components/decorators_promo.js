import React from 'react'
import './carouselDecorator.scss'

const decorators_promo = [{
  component: class Left extends React.Component{
    render() {
      return (
        <button className="carousel--left" onClick={this.props.previousSlide}></button>
      )
    }
  },
  position: 'CenterLeft',
  style: {
    left: '-30px',
    top: '52%'
  }
},
{
  component: class Right extends React.Component{
    render() {
      // console.log(this.props.slideCount + ' - '+this.props.slidesToScroll + ' - ' + this.props.slidesToShow);
      return (
        <button className="carousel--right" onClick={this.props.nextSlide}></button>
      )
    }
  },
  position: 'CenterRight',
  style: {
    right: '-30px',
    top: '52%'
  }
},
{
    component: class Dots extends React.Component{
      render() {
        var self = this;
        var indexes = this.getIndexes(self.props.slideCount, self.props.slidesToScroll);
        return (
          <ul style={self.getListStyles()}>
            {
              indexes.map(function(index) {
                return (
                  <li style={self.getListItemStyles()} key={index}>
                    <button className="slider-dots"
                      style={self.getButtonStyles(self.props.currentSlide === index)}
                      onClick={self.props.goToSlide.bind(null, index)}>
                    </button>
                  </li>
                )
              })
            }
          </ul>
        )
      }
      getIndexes(count, inc) {
        var arr = [];
        for (var i = 0; i < count; i += inc) {
          arr.push(i);
        }
        return arr;
      }
      getListStyles() {
        return {
          position: 'relative',
          margin: 0,
          top: 30,
          padding: 0
        }
      }
      getListItemStyles() {
        return {
          listStyleType: 'none',
          display: 'inline-block'
        }
      }
      getButtonStyles(active) {
        return {
          border: 0,
          width: '12px',
          height: '12px',
          background: active ? '#ff5722' : '#FFF',
          borderRadius: '50%',
          marginRight: '8px',
          boxShadow: '0 1px 8px 0 rgba(0, 0, 0, 0.12)',
        }
      }
    },
    position: 'BottomLeft'
  }
];

export default decorators_promo;
