import React from 'react'
import Carousel from 'nuka-carousel'
import decorators_popDest from './decorators_popDest.js'
import decorators_promo from './decorators_promo.js'
import './HomeView.scss'

import img1 from '../assets/Flight-Banner-2.png'

import img_dest1 from '../assets/Flight-Banner-2-05.png'
import img_dest2 from '../assets/Flight-Banner-2-06.png'
import img_dest3 from '../assets/Flight-Banner-2-07.png'
import img_dest4 from '../assets/Flight-Banner-2-08.png'
import img_dest5 from '../assets/Flight-Banner-2-09.png'
import img_dest6 from '../assets/Flight-Banner-2-11.png'

import img_advan1 from '../assets/Flight-illustraion-1-13.png'
import img_advan2 from '../assets/Flight-illustraion-2-13.png'
import img_advan3 from '../assets/Flight-illustraion-3-13.png'
import img_advan4 from '../assets/Flight-illustraion-4-13.png'

import banner3 from '../assets/Flight-Banner-03.png'

import Filter from 'components/Filter'

class HomeView extends React.Component {
  render(){
    return (
      <div className="flight__home__wrapper">
        <div className="flight__home__top-banner">
          <img src={banner3}/>
        </div>
        <div className="flight__home__main-title">
          Pesan Tiket Pesawatmu di Tokopedia Sekarang!
        </div>

        <Filter />

        <div className="flight__home__promo-banner mb-50">
          <Carousel slideWidth="950px" cellSpacing={15} decorators={decorators_promo} wrapAround={true}>
            <img className="bradius9" src={img1} />
            <img className="bradius9" src={img1} />
            <img className="bradius9" src={img1} />
          </Carousel>
        </div>
        <div className="flight__home__white-wrapper">
          <div className="flight__home__dest-pop">
            <div className="dest-pop__title">
              Destinasi Populer
            </div>
            <div className="dest-pop__slider">
              <Carousel slidesToShow={6} slidesToScroll={3} cellSpacing={15} decorators={decorators_popDest}>
                <img className="bradius9" src={img_dest1}/>
                <img className="bradius9" src={img_dest2}/>
                <img className="bradius9" src={img_dest3}/>
                <img className="bradius9" src={img_dest4}/>
                <img className="bradius9" src={img_dest5}/>
                <img className="bradius9" src={img_dest6}/>
                <img className="bradius9" src={img_dest1}/>
                <img className="bradius9" src={img_dest2}/>
              </Carousel>
            </div>
          </div>
          <div className="flight__home__advantage">
            <div className="home-advantage__title">
              Mengapa Pesan Tiket Pesawat di Tokopedia
            </div>
            <div className="home-advantage__content">
              <div className="home-advantage__content-item">
                <img src={img_advan1}/>
                <div className="content-item__title">
                  Transaksi Aman
                </div>
                <div className="fs-12">
                  Keamanan dan privasi transaksi online dilindungi oleh teknologi SSL dan konfirmasi pembayaran serta  e-tiket secara instan dikirim  ke email Anda.
                </div>
              </div>
              <div className="home-advantage__content-item">
                <img src={img_advan2}/>
                <div className="content-item__title">
                  Bandingkan Harga Termurah
                </div>
                <div className="fs-12">
                  Tersedia harga terbaik dari berbagai sumber dan menawarkan jutaan penerbangan untuk menemukan  kesepakatan termurah.
                </div>
              </div>
              <div className="home-advantage__content-item">
                <img src={img_advan3}/>
                <div className="content-item__title">
                  Berbagai Metode Pembayaran
                </div>
                <div className="fs-12">
                  Transaksi mudah dengan berbagai pembayaran, seperti Saldo Tokopedia, BCA KlikPay, Mandiri Clickpay, mandiri e-cash, dan Kartu Kredit.
                </div>
              </div>
              <div className="home-advantage__content-item">
                <img src={img_advan4}/>
                <div className="content-item__title">
                  Layanan Pelanggan 24/7
                </div>
                <div className="fs-12">
                  Reservasi semakin mudah! Pemesanan Tiket Pesawat dapat dilakukan kapan saja dan di mana saja. 24 jam sehari dan 7 hari seminggu.
                </div>
              </div>
            </div>
          </div>
          <hr className="max_1200"></hr>
          <div className="flight__home__seo-wrapper">
            <div className="flight__home__seo-item">
              <div className="seo-item__title">
                Beli Tiket Pesawat Kini Lebih Praktis, Tidak Perlu Antri!
              </div>
              <div className="seo-item__content">
                Sering kerepotan saat harus membeli tiket kereta api di stasiunnya langsung? Sekarang tidak perlu lagi mengantri di stasiun kereta, karena di Tokopedia Anda bisa memesan tiket kereta api secara online, kapan saja, dan di mana saja. Anda juga tidak perlu takut kehabisan tiket kereta api karena bisa pesan tiket dari jauh-jauh hari. Tinggal pesan tiket di Tokopedia, cetak tiket dengan menunjukkan kode booking, dan langsung berangkat ke tempat tujuan. Lebih mudah dan praktis, bukan? Liburan jadi lebih menyenangkan karena Anda tidak perlu buang-buang waktu dan tenaga untuk antri membeli tiket.
                <br/><br/>
                Metode pembayaran yang bisa Anda gunakan untuk membeli tiket kereta api pun sangat lengkap, mulai dari Kartu Kredit, BCA KlikPay, Mandiri ClickPay, Mandiri e-cash, hingga pembayaran dengan Saldo Tokopedia. Kalau bisa beli tiket kereta api online, kenapa harus capek-capek antri di stasiun? Anda juga bisa menikmati berbagai promo tiket kereta api yang menarik dari Tokopedia, lho. Jadi tunggu apa lagi? Ayo pesan tiket kereta api online di Tokopedia, mudah dan praktis!
              </div>
            </div>
            <div className="flight__home__seo-item">
              <div className="seo-item__title">
                Rasakan Manfaat Beli Tiket Pesawat di Tokopedia
              </div>
              <div className="seo-item__content">
                Bepergian bersama keluarga dan sahabat merupakan saat-saat yang menyenangkan. Salah satu momen yang bisa membangun mood bepergian adalah saat-saat selama perjalanan ke tempat tujuan. Jangan sampai momen ini rusak dan mengganggu semangat jalan-jalan Anda akibat duduk berjauhan atau terpisah dari keluarga dan sahabat. Tidak hanya perasaan kurang nyaman dan aman karena duduk berdekatan dengan orang yang tidak dikenal, posisi duduk yang berjauhan ini juga bisa menyebabkan sulitnya berkomunikasi dengan teman perjalanan. Akibatnya, Anda bisa kehabisan ide harus melakukan aktivitas apa untuk menghabiskan waktu dan mati gaya selama perjalanan.
                <br/><br/>
                Dengan membeli tiket kereta api online di Tokopedia, Anda bisa terhindar dari masalah ini, Toppers. Anda bisa pilih sendiri tempat duduk yang diinginkan dan bebas dari risiko terpisah dari orang terdekat. Pembelian tiket kereta online ini didukung oleh berbagai pembayaran instan dengan pilihan rute perjalanan dan kelas yang lengkap. Anda juga bisa pilih tiket online di Tokopedia berdasarkan harga termurah, baik untuk perjalanan solo Anda ataupun dengan orang-orang tersayang. Beli tiket kereta api online di Tokopedia, rasakan kemudahan dan kenyamanannya!
              </div>
            </div>
            <div className="flight__home__seo-item">
              <div className="seo-item__title">
                Cek Jadwal Pesawat dan Dapatkan Harga Tiket Termurah
              </div>
              <div className="seo-item__content">
                Jangan bingung jika ingin mengetahui jadwal kereta api dan harga tiket KA ketika tidak berada di depan komputer. Cukup periksa dari smartphone Anda untuk mendapatkan jadwal kereta, lengkap dengan harga terbaik. Semua bisa langsung tanpa perlu ke stasiun atau membandingkan harga di travel agency. Tokopedia bekerja sama dengan PT. Kereta Api Indonesia memastikan pelayanan cepat, dari proses pembelian tiket, check-in hingga duduk di dalam kereta api pilihan Anda.
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default HomeView
