import React from 'react'
import { IndexLink, Link } from 'react-router'
import PropTypes from 'prop-types'
import './PageLayout.scss'
import '../../styles/digital.scss'
import '../../styles/utilities.scss'

import Header from 'components/Header'
import Footer from 'components/Footer'

class PageLayout extends React.Component {

  props: {
    children: node
  }

  state = {
    overlayVisible: false,
    bridgeWidgetVisible: false,
    userProfileVisible: false
  }

  onHoverUserIcon = () => {
    this.setState({
      userProfileVisible: true,
      overlayVisible: true
    })
  }

  onLeaveUserIcon = () => {
    this.setState({
      userProfileVisible: false,
      overlayVisible: false
    })
  }

  onHoverBridgeIcon = () => {
    this.setState({
      bridgeWidgetVisible: true,
      overlayVisible: true
    })
  }

  onLeaveBridgeIcon = () => {
    this.setState({
      bridgeWidgetVisible: false,
      overlayVisible: false
    })
  }

  render() {
    return (
      <div>

        { this.state.overlayVisible && <div className='overlay'></div> }

        <Header profileVisible={this.state.userProfileVisible}
                bridgeWidgetVisible={this.state.bridgeWidgetVisible}
                onHoverUserIcon={this.onHoverUserIcon}
                onLeaveUserIcon={this.onLeaveUserIcon}
                onHoverBridgeIcon={this.onHoverBridgeIcon}
                onLeaveBridgeIcon={this.onLeaveBridgeIcon} />


        {this.props.children}


        <Footer />
      </div>
    );
  }
}

export default PageLayout
